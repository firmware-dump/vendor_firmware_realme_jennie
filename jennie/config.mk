# C.15 Firmwares
FIRMWARE_IMAGES := \
    abl \
    aop \
    aop_config \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    hyp \
    imagefv \
    keymaster \
    modem \
    oplus_sec \
    oplusstanvbk \
    qupfw \
    shrm \
    splash \
    tz \
    uefi \
    uefisecapp \
    xbl \
    xbl_config \
    xbl_ramdump

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
