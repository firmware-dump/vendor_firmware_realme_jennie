# C.08 Firmwares
FIRMWARE_IMAGES := \
    BTFM \
    DRIVER \
    abl \
    cmnlib \
    cmnlib64 \
    devcfg \
    dpAP \
    dspso \
    hyp \
    imagefv \
    keymaster64 \
    modem \
    oppo_sec \
    qupv3fw \
    rpm \
    splash \
    static_nvbk \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config
